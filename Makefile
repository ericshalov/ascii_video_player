#
#	Makefile for ascii_video_player
#

# Linux:
CC = gcc
CCFLAGS = -Wall
LDFLAGS = -lvlc

# MacOS/X:
# (First download and install VLC into /Applications)
#CC = cc
#CCFLAGS = -I/Applications/VLC.app/Contents/MacOS/include/
#LDFLAGS = -L/Applications/VLC.app/Contents/MacOS/lib -lvlc -lvlccore
# To execute, first set:
# export DYLD_LIBRARY_PATH=/Applications/VLC.app/Contents/MacOS/lib

all: ascii_video_player

ascii_video_player: ascii_video_player.o
	$(CC) $(CCFLAGS) -o ascii_video_player ascii_video_player.o $(LDFLAGS)
	
ascii_video_player.o: ascii_video_player.c
	$(CC) $(CCFLAGS) -o ascii_video_player.o -c ascii_video_player.c
	
clean:
	rm -f ascii_video_player ascii_video_player.o
