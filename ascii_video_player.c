/*
  ascii_video_player.c
  Uses libVLC to decode a video file and display it using ASCII characters on a terminal.
  Requires libVLC. Tested with libVLC 1.1.1.
  
  (C) Copyright 2012 Eric Shalov. All Rights Reserved.
  ascii_video_player is licensed under the BSD 3-Clause License.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

  * Neither the name of the {organization} nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vlc/libvlc.h>
#include <vlc/libvlc_media.h>
#include <vlc/libvlc_media_player.h>

/* prototypes */
void *lock_cb(void *opaque, void **planes);
void unlock_cb(void *data, void *picture, void *const *p_pixels);
void display_cb(void *opaque, void *picture);
unsigned int *format_cb(void **opaque, char *chroma, unsigned *width, unsigned *height, unsigned *pitches, unsigned *lines);
void *cleanup_cb(void *opaque);

libvlc_media_player_t *player;
libvlc_media_t *m;

int panel_width = 640, panel_height = 480;

int main(int argc, char *argv[]) {
  libvlc_instance_t *i;
  unsigned char *opaque = NULL;
  char *path = NULL;

  if(argc == 2) {
    path = argv[1];
  } else {
    fprintf(stderr,"Usage: %s [video-filename]\n", argv[0]);
    exit(1);
  }  

  opaque = NULL;
  
  if( ! (i = libvlc_new(0, NULL)) ) {
    printf("Error initiating instance.\n");
  }
  
  printf("\033[2JVLC version: %s\n", libvlc_get_version() );
  
  if( ! (m = libvlc_media_new_path (i, path)) ) {
    printf("Error opening media file.\n");
  }
  
  libvlc_media_parse(m);

  
  printf("Title: %s\n", libvlc_media_get_meta(m, libvlc_meta_Title));
  printf("Duration: %ldms\n", libvlc_media_get_duration(m) );
  
  player = libvlc_media_player_new_from_media(m);
  libvlc_video_set_format(player, "RGBA", panel_width, panel_height, panel_width*4);
  
  if( libvlc_media_player_play(player) != 0 ) {
    printf("Unable to play.\n");
    exit(1);
  }


  libvlc_media_player_set_position(player, 0.000);
  libvlc_media_player_set_time(player, 0);
  libvlc_media_player_set_rate(player, 1.0);  /* 1.0 = normal = 100% */
  
/*  libvlc_video_set_format_callbacks(player, format_cb, cleanup_cb);*/
  libvlc_video_set_callbacks(player, lock_cb, unlock_cb, display_cb, opaque);

  while(1) {
//    libvlc_media_player_next_frame(player);
    usleep(30000);
  }
  
  libvlc_media_player_release(player);
  libvlc_media_release(m);
  libvlc_release(i);
}

unsigned char *p;

void *lock_cb(void *opaque, void **planes) {
  *planes = malloc(panel_width*panel_height*4);
  
  p = *planes;
  
  return NULL;
}

void unlock_cb(void *data, void *picture, void *const *p_pixels) {
}   

void display_cb(void *opaque, void *picture) {
  int x,y;
  char *in = " _.,:;%@";
  int g;
  libvlc_media_stats_t stats;
  
  if( ! libvlc_media_get_stats(m, &stats) ) {
    printf("Error getting media stats.\n");
  }
  
  printf("\033[4;1H%s At Timecode %ldms / %ldms, position %.3f\n"
         "Dropped: %d frames, Frame rate: %5.2f fps Playback: %5.1f%%\n",
      libvlc_media_player_is_playing(player)==1 ? "PLAY" : "STOP",
      libvlc_media_player_get_time(player),
      libvlc_media_player_get_length(player),
      libvlc_media_player_get_position(player),
      stats.i_lost_pictures,
      libvlc_media_player_get_fps(player),
      libvlc_media_player_get_rate(player)*100.0);

  
  for(y=0;y<panel_height;y+=16) {
    printf("[ ");
    for(x=0;x<panel_width;x+=7) {
      g = ( p[(y*panel_width+x)*4+0]
          + p[(y*panel_width+x)*4+1]
          + p[(y*panel_width+x)*4+2]
          ) / 3;
        
      printf("%c", in[g/32]);
    }
    printf(" ]\n");
  }
}

unsigned int *format_cb(void **opaque, char *chroma, unsigned *width, unsigned *height, unsigned *pitches, unsigned *lines) {
  printf("format_cb()\n");
  return NULL;
}

void *cleanup_cb(void *opaque) {
  printf("cleanup_cb()\n");
  return NULL;
}
